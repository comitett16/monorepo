NODE := "docker compose exec node"
PNPM := NODE + " pnpm"

ace *args:
	{{PNPM}} -F "webapp" run ace {{args}}

dev *args:
	{{PNPM}} -F "webapp" dev {{args}}

pnpm *args:
	{{PNPM}} {{args}}

run *args:
	{{PNPM}} run {{args}}

app *args:
	{{PNPM}} -F "webapp" {{args}}

doc *args:
	{{PNPM}} -F "documentation" {{args}}

smartping *args:
	{{PNPM}} -F "smartping" {{args}}
