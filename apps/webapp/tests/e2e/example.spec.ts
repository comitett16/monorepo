import { test } from '@japa/runner';

test('has title', async ({ visit }) => {
	const page = await visit('/inertia');

	await page.assertTitle('Comité Charente TT');
});
