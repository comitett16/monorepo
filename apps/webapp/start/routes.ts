import router from '@adonisjs/core/services/router';

import { uuidRegex } from '#common/controllers/uuid_param';
import { middleware } from '#start/kernel';

const uuidMatcher = { match: uuidRegex };

// region Controller imports
const LoginFormController = () => import('#security/user/actions/login_form/controller');
const LoginController = () => import('#security/user/actions/login/controller');
const LogoutController = () => import('#security/user/actions/logout/controller');
const RegisterController = () => import('#security/user/actions/register/controller');
const CreateInvitationController = () =>
	import('#security/invitation/actions/create_invitation/controller');
const AcceptInvitationController = () =>
	import('#security/invitation/actions/accept_invitation/controller');
const TestController = () => import('#security/user/actions/test/controller');
// endregion

// region Authentication routes
router.get('/auth/login', [LoginFormController]).as('auth.login.form');
router.post('/auth/login', [LoginController]).as('auth.login.submit');
router.post('/auth/register', [RegisterController]).as('auth.register');
router.post('/auth/logout', [LogoutController]).as('auth.logout');
// endregion

// region Invitation routes
router
	.post('/invite', [CreateInvitationController])
	.as('invite.create')
	.middleware(middleware.auth());

router
	.post('/invite/:uuid', [AcceptInvitationController])
	.where('uuid', uuidMatcher)
	.as('invite.accept');
// endregion

router.get('/inertia', [TestController]).as('inertia.test');
