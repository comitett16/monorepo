import router from '@adonisjs/core/services/router';
import server from '@adonisjs/core/services/server';

server.errorHandler(() => import('#common/exceptions/handler'));

server.use([
	() => import('#common/middlewares/container_bindings_middleware'),
	() => import('@adonisjs/static/static_middleware'),
	() => import('@adonisjs/cors/cors_middleware'),
	() => import('@adonisjs/vite/vite_middleware'),
	() => import('@adonisjs/inertia/inertia_middleware'),
]);

router.use([
	() => import('@adonisjs/core/bodyparser_middleware'),
	() => import('@adonisjs/session/session_middleware'),
	() => import('@adonisjs/shield/shield_middleware'),
	() => import('@adonisjs/auth/initialize_auth_middleware'),
	() => import('#security/user/middlewares/silent_auth_middleware'),
	() => import('#security/user/middlewares/initialize_bouncer_middleware'),
]);

export const middleware = router.named({
	auth: () => import('#security/user/middlewares/auth_middleware'),
});
