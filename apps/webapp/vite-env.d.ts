/// <reference types="vite/client" />

interface ImportMetaEnv {
	readonly VITE_APP_TITLE: string;
	readonly TZ: string;
	readonly PORT: number;
	readonly HOST: string;
	readonly LOG_LEVEL: string;
	readonly APP_NAME: string;
	readonly APP_KEY: string;
	readonly NODE_ENV: string;
	readonly SESSION_DRIVER: string;
	readonly DB_HOST: string;
	readonly DB_PORT: number;
	readonly DB_USER: string;
	readonly DB_PASSWORD: string;
	readonly DB_DATABASE: string;
	readonly SENTRY_DSN: string;
	readonly BREVO_API_KEY: string;
	readonly MAILING_SENDER_NAME: string;
	readonly MAILING_SENDER_EMAIL: string;
	readonly MAILING_REPLY_TO_NAME: string;
	readonly MAILING_REPLY_TO_EMAIL: string;
	readonly SMTP_HOST: string;
	readonly SMTP_PORT: number;
	readonly SMTP_USERNAME: string;
	readonly SMTP_PASSWORD: string;
	readonly CI: string;
}

interface ImportMeta {
	readonly env: ImportMetaEnv
}
