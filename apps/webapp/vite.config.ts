/// <reference types="vitest" />
import { defineConfig } from 'vite';
import inertia from '@adonisjs/inertia/client';
import react from '@vitejs/plugin-react-swc';
import adonisjs from '@adonisjs/vite/client';
import UnoCSS from 'unocss/vite';
import { getDirname } from '@adonisjs/core/helpers';

export default defineConfig({
	plugins: [
		inertia({ ssr: { enabled: false } }),
		react(),
		adonisjs({
			entrypoints: ['resources/css/app.css', 'resources/js/app.ts', 'inertia/main.tsx'],
			reload: ['resources/views/**/*.edge'],
		}),
		UnoCSS(),
	],
	test: {
		globals: true,
		environment: 'jsdom',
		setupFiles: './inertia/tests/setup.ts',
		include: ['./inertia/**/*.{spec,e2e}.tsx'],
	},
	resolve: {
		alias: {
			'~/': `${getDirname(import.meta.url)}/inertia/`,
		},
	},
	server: {
		host: true,
		hmr: {
			clientPort: 443,
		},
	},
});
