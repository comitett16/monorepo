/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Serialize } from '@tuyau/utils/types';
import type { ReactNode } from 'react';

import type ControllerInterface from '#common/controllers/controller_interface';

type InferPageProps<Controller, Method extends keyof Controller> = Controller[Method] extends (
	...args: any[]
) => any
	? Serialize<Exclude<Awaited<ReturnType<Controller[Method]>>, string | void>['props']>
	: never;

export type ControllerProps<
	Controller extends ControllerInterface,
	Method extends keyof Controller = 'handle',
> = InferPageProps<Controller, Method>;

export type Component = ReactNode;

/* eslint-enable @typescript-eslint/no-explicit-any */
