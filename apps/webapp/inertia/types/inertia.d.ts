import type User from '../../domains/security/models/user';

declare module '@inertiajs/core' {
	export interface PageProps {
		errors: Record<string, string[]>;
		user: User | undefined;
	}
}
