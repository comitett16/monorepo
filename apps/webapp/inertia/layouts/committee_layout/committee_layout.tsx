import type { PropsWithChildren } from 'react';

import { Link } from '@inertiajs/react';
import {
	ActionIcon,
	AppShell,
	Burger,
	Group,
	Menu,
	Text,
	Tooltip,
	useMantineColorScheme,
} from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import {
	PiArrowElbowDownRight,
	PiBuildings,
	PiEnvelopeOpen,
	PiFolders,
	PiHouseLine,
	PiLaptop,
	PiLock,
	PiMapPin,
	PiMoon,
	PiPersonSimpleRun,
	PiPhoneCall,
	PiSignOut,
	PiSun,
	PiUser,
	PiUsers,
	PiUsersFour,
} from 'react-icons/pi';

import { NotificationsList, SidebarLink } from '~/components';
import { iconProps } from '~/utils';

type ComponentProps = PropsWithChildren;

export function CommitteeLayout(props: ComponentProps) {
	const [mobileOpened, { toggle: toggleMobile }] = useDisclosure();
	const [desktopOpened, { toggle: toggleDesktop }] = useDisclosure(true);

	return (
		<AppShell
			header={{ height: 48 }}
			navbar={{
				width: 300,
				breakpoint: 'sm',
				collapsed: { mobile: !mobileOpened, desktop: !desktopOpened },
			}}
			padding="md"
		>
			<Header
				desktopOpened={desktopOpened}
				mobileOpened={mobileOpened}
				toggleDesktop={toggleDesktop}
				toggleMobile={toggleMobile}
			/>
			<Sidebar />
			<AppShell.Main>{props.children}</AppShell.Main>
		</AppShell>
	);
}

function Header(props: {
	mobileOpened: boolean;
	desktopOpened: boolean;
	toggleMobile: () => void;
	toggleDesktop: () => void;
}) {
	return (
		<>
			<AppShell.Header display="flex" className="items-center justify-between pr-1">
				<Group h="100%" px="md">
					<Burger
						opened={props.mobileOpened}
						onClick={props.toggleMobile}
						hiddenFrom="sm"
						size="sm"
					/>
					<Tooltip
						label={`${props.desktopOpened ? 'Cacher' : 'Afficher'} la barre latérale`}
						visibleFrom="sm"
						position="right"
					>
						<Burger
							opened={props.desktopOpened}
							onClick={props.toggleDesktop}
							visibleFrom="sm"
							size="sm"
						/>
					</Tooltip>
					<Text fw="600">Comité Charente TT</Text>
				</Group>
				<Group gap="6" className="items-center">
					<NotificationsList />
					<ColorThemeSwitcher />
					<UserSettings />
				</Group>
			</AppShell.Header>
		</>
	);
}

function Sidebar() {
	return (
		<AppShell.Navbar>
			<SidebarLink href="#!" label="Tableau de bord" leftSection={<PiHouseLine {...iconProps} />} />

			<SidebarLink
				href="#!"
				label="Sportif"
				leftSection={<PiPersonSimpleRun {...iconProps} />}
				childrenOffset={32}
				defaultOpened
			>
				<SidebarLink
					href="#!"
					label="Championnat par équipes"
					leftSection={<PiArrowElbowDownRight {...iconProps} />}
				/>
				<SidebarLink
					href="#!"
					label="Critérium Fédéral"
					leftSection={<PiArrowElbowDownRight {...iconProps} />}
				/>
				<SidebarLink
					href="#!"
					label="Championnat Jeunes"
					leftSection={<PiArrowElbowDownRight {...iconProps} />}
				/>
				<SidebarLink
					href="#!"
					label="Coupe du Comité"
					leftSection={<PiArrowElbowDownRight {...iconProps} />}
				/>
				<SidebarLink
					href="#!"
					label="Challenge Féminin"
					leftSection={<PiArrowElbowDownRight {...iconProps} />}
				/>
			</SidebarLink>

			<SidebarLink
				href="#!"
				label="Administration"
				leftSection={<PiFolders {...iconProps} />}
				childrenOffset={32}
			>
				<SidebarLink href="#!" label="Clubs" leftSection={<PiBuildings {...iconProps} />} />
				<SidebarLink href="#!" label="Licenciés" leftSection={<PiUsers {...iconProps} />} />
				<SidebarLink href="#!" label="Contacts" leftSection={<PiPhoneCall {...iconProps} />} />
				<SidebarLink href="#!" label="Salles" leftSection={<PiMapPin {...iconProps} />} />
			</SidebarLink>

			<SidebarLink
				href="#!"
				label="Permissions"
				leftSection={<PiLock {...iconProps} />}
				childrenOffset={32}
			>
				<SidebarLink href="#!" label="Utilisateurs" leftSection={<PiUsersFour {...iconProps} />} />
				<SidebarLink
					href="#!"
					label="Invitations"
					leftSection={<PiEnvelopeOpen {...iconProps} />}
				/>
			</SidebarLink>
		</AppShell.Navbar>
	);
}

function ColorThemeSwitcher() {
	const { setColorScheme, colorScheme } = useMantineColorScheme();

	return (
		<Menu shadow="md">
			<Menu.Target>
				<Tooltip label="Thème">
					<ActionIcon variant="default" size="lg" aria-label="Modifier le thème">
						{colorScheme === 'light' ? (
							<PiSun {...iconProps} />
						) : colorScheme === 'dark' ? (
							<PiMoon {...iconProps} />
						) : (
							<PiLaptop {...iconProps} />
						)}
					</ActionIcon>
				</Tooltip>
			</Menu.Target>

			<Menu.Dropdown>
				<Menu.Item leftSection={<PiSun {...iconProps} />} onClick={() => setColorScheme('light')}>
					Clair
				</Menu.Item>
				<Menu.Item leftSection={<PiMoon {...iconProps} />} onClick={() => setColorScheme('dark')}>
					Sombre
				</Menu.Item>
				<Menu.Item leftSection={<PiLaptop {...iconProps} />} onClick={() => setColorScheme('auto')}>
					Automatique
				</Menu.Item>
			</Menu.Dropdown>
		</Menu>
	);
}

function UserSettings() {
	return (
		<>
			<Tooltip label="Profil">
				<ActionIcon variant="default" size="lg" aria-label="Réglages" component={Link} href="#!">
					<PiUser {...iconProps} />
				</ActionIcon>
			</Tooltip>
			<Tooltip label="Déconnexion">
				<ActionIcon variant="default" size="lg" aria-label="Réglages" component={Link} href="#!">
					<PiSignOut {...iconProps} />
				</ActionIcon>
			</Tooltip>
		</>
	);
}
