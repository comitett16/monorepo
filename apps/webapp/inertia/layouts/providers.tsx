import { MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';
import { Notifications } from '@mantine/notifications';
import { Spotlight } from '@mantine/spotlight';
import { PropsWithChildren, StrictMode } from 'react';

import { colorSchemeManager, theme } from '~/theme';

export function Providers(props: PropsWithChildren) {
	return (
		<StrictMode>
			<MantineProvider
				theme={theme}
				colorSchemeManager={colorSchemeManager}
				defaultColorScheme="dark"
			>
				<ModalsProvider>
					<Notifications />
					<Spotlight actions={[]} />
					{props.children}
				</ModalsProvider>
			</MantineProvider>
		</StrictMode>
	);
}
