import { cleanup } from '@testing-library/react';
import { afterEach } from 'vitest';
import '@testing-library/jest-dom/vitest';

// expect.extend(matchers);

afterEach(() => {
	cleanup();
});
