import { ActionIcon, Drawer, Indicator, Tooltip } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { PiBell } from 'react-icons/pi';

import { iconProps } from '~/utils';

export function NotificationsList() {
	const [notificationsOpened, { open: openNotifications, close: closeNotifications }] =
		useDisclosure(false);

	return (
		<>
			<Tooltip label="Notifications">
				<Indicator
					color="red"
					label="9"
					size={14}
					offset={4}
					fz="xs"
					position="top-start"
					className="flex items-center"
				>
					<ActionIcon variant="default" size="lg" aria-label="Réglages" onClick={openNotifications}>
						<PiBell {...iconProps} />
					</ActionIcon>
				</Indicator>
			</Tooltip>
			<Drawer
				offset={8}
				position="right"
				radius="md"
				opened={notificationsOpened}
				onClose={closeNotifications}
				title="Notifications"
			>
				<p>hello</p>
			</Drawer>
		</>
	);
}
