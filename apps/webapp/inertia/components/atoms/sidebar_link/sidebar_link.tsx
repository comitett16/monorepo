import type { InertiaLinkProps } from '@inertiajs/react';

import { Link } from '@inertiajs/react';
import { NavLink, NavLinkProps } from '@mantine/core';

type ComponentProps = Omit<InertiaLinkProps, 'children'> & NavLinkProps;

export function SidebarLink(props: ComponentProps) {
	return <NavLink component={Link} {...props} />;
}
