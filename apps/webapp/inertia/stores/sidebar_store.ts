import { create } from 'zustand';
import { persist } from 'zustand/middleware';

type SidebarStore = {
	isOpen: boolean;
	toggle: () => void;
	close: () => void;
	open: () => void;
};

const useSidebarStore = create(
	persist<SidebarStore>(
		(set) => ({
			isOpen: true,
			toggle: () => set((state) => ({ isOpen: !state.isOpen })),
			close: () => set({ isOpen: false }),
			open: () => set({ isOpen: true }),
		}),
		{
			name: 'sidebar',
		},
	),
);

export { useSidebarStore as useSidebar };
