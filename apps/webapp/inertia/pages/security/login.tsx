import { router } from '@inertiajs/react';
import {
	Anchor,
	Button,
	Checkbox,
	Container,
	Paper,
	PasswordInput,
	Text,
	TextInput,
	Title,
} from '@mantine/core';
import { useForm } from '@tanstack/react-form';
import { FormEvent } from 'react';

import { GlobalLayout } from '~/layouts';
import { Component } from '~/types/utils';

function Login() {
	const form = useForm({
		defaultValues: {
			identifier: '',
			password: '',
			remember: false,
		},
		onSubmit: ({ value }) => {
			router.post('/auth/login', value);
		},
	});

	const handleFormSubmit = (event: FormEvent) => {
		event.preventDefault();
		event.stopPropagation();
		// eslint-disable-next-line @typescript-eslint/no-floating-promises
		form.handleSubmit();
	};

	return (
		<Container size={420} my={40}>
			<Title ta="center">Connexion utilisateur</Title>
			<Text c="dimmed" size="xl" ta="center" mt={5}>
				Comité Charente TT
			</Text>

			<Paper withBorder shadow="md" p={30} mt={30} radius="md">
				<form onSubmit={handleFormSubmit}>
					<form.Field
						name="identifier"
						children={(field) => (
							<TextInput
								name={field.name}
								label="Numéro de licence"
								placeholder="123456"
								value={field.state.value}
								onBlur={field.handleBlur}
								onChange={(event) => field.handleChange(event.target.value)}
								autoFocus
								required
							/>
						)}
					/>
					<form.Field
						name="password"
						children={(field) => (
							<PasswordInput
								name={field.name}
								label="Mot de passe"
								mt="md"
								value={field.state.value}
								onBlur={field.handleBlur}
								onChange={(event) => field.handleChange(event.target.value)}
								required
							/>
						)}
					/>
					<form.Field
						name="remember"
						children={(field) => (
							<Checkbox
								name={field.name}
								label="Se souvenir de moi pendant une semaine"
								mt="md"
								checked={field.state.value}
								onBlur={field.handleBlur}
								onChange={(event) => field.handleChange(event.target.checked)}
							/>
						)}
					/>
					<form.Subscribe
						selector={(state) => [state.canSubmit, state.isSubmitting]}
						children={([canSubmit, isSubmitting]) => (
							<Button type="submit" disabled={!canSubmit} fullWidth mt="xl" size="md">
								{isSubmitting ? '...' : 'Connexion'}
							</Button>
						)}
					/>
				</form>

				<Anchor component="button" size="sm" mt={16} ta="center" w="100%">
					Mot de passe oublié ?
				</Anchor>
			</Paper>
		</Container>
	);
}

Login.layout = (page: Component) => <GlobalLayout children={page} />;

export default Login;
