// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function ServerError(props: { error: any }) {
	return (
		<>
			<div className="container">
				<div className="title">Server Error</div>
				{/* eslint-disable-next-line @typescript-eslint/no-unsafe-member-access */}
				<span>{props.error.message}</span>
			</div>
		</>
	);
}
