import type RegisterController from '#security/user/actions/register/controller';

import { CommitteeLayout } from '~/layouts/committee_layout/committee_layout';
import { Component, ControllerProps } from '~/types/utils';

function Home(props: ControllerProps<RegisterController>) {
	return (
		<>
			<div className="container">
				<h1>Playwright</h1>
				<div className="title">Adonis Incroyable {props.data?.userId} x Inertia x React</div>
				<p>Sphinx of black quartz, judge my vow.</p>

				<span>
					Learn more about AdonisJS and Inertia.js by visiting the{' '}
					<a href="https://docs.adonisjs.com/guides/inertia">AdonisJS documentation</a>.
				</span>
			</div>
		</>
	);
}

Home.layout = (page: Component) => <CommitteeLayout children={page} />;

export default Home;
