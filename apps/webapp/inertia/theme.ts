import { createTheme, localStorageColorSchemeManager } from '@mantine/core';

export const theme = createTheme({
	primaryColor: 'violet',
	fontFamily: '"Manrope Variable", sans-serif',
	headings: {
		textWrap: 'balance',
	},
});

export const colorSchemeManager = localStorageColorSchemeManager({ key: 'mantine-color-scheme' });
