import { rem } from '@mantine/core';

export const iconProps = { style: { width: rem(18), height: rem(18) } };
