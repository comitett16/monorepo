// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../adonisrc.ts" />

// region Styles import
import '@fontsource-variable/manrope';
import '@unocss/reset/tailwind.css';
import '@mantine/core/styles.css';
import '@mantine/notifications/styles.css';
import '@mantine/spotlight/styles.css';
import '@mantine/dropzone/styles.css';
import '@mantine/tiptap/styles.css';
import 'virtual:uno.css';
import './css/index.css';
// endregion

import { resolvePageComponent } from '@adonisjs/inertia/helpers';
import { createInertiaApp } from '@inertiajs/react';
import { createRoot } from 'react-dom/client';

import { Providers } from '~/layouts/providers';

const appName = 'Comité Charente TT';

void createInertiaApp({
	progress: { color: '#5468FF' },

	title: (title) => `${title} - ${appName}`,

	resolve: async (name) => {
		return resolvePageComponent(`./pages/${name}.tsx`, import.meta.glob('./pages/**/*.tsx'));
	},

	setup({ el, App, props }) {
		el.classList.add('h-full');
		createRoot(el).render(
			<Providers>
				<App {...props} />
			</Providers>,
		);
	},
});
