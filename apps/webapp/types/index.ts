/* eslint-disable @typescript-eslint/no-explicit-any */
import type { DateTime } from 'luxon';

// Matches any primitive value.
export type Primitive = null | string | number | boolean | DateTime;

// Get all values of an object as a union type.
export type ValueOf<T> = T[keyof T];

// Get all values of an object as a union type, including nested objects.
export type DeepValueOf<T extends Record<string, unknown>, Key = keyof T> = Key extends string
	? T[Key] extends Record<string, unknown>
		? DeepValueOf<T[Key]>
		: ValueOf<T>
	: never;

// NullableObject<T> is an object with the same keys as T, but with all values being nullable.
export type NullableObject<T> = {
	[K in keyof T]: T[K] | null;
};

// Extract the instance type of class or the prototype of a class-like object.
export type ExtractInstanceType<T> = T extends new (...args: any[]) => infer R
	? R
	: T extends {
				prototype: infer P;
		  }
		? P
		: any;

// Get all method names of a class as a union type.
export type ExtractMethodNames<T> = {
	[K in keyof T]: T[K] extends (...args: any[]) => any ? K : never;
}[keyof T];

// Get all methods signatures of a class as a union type.
export type ExtractMethods<T> = Pick<T, ExtractMethodNames<T>>;

// Get all property names of a class as a union type, except those with keys starting with a $ sign prefix.
export type ExtractProperties<T> = {
	[K in keyof T]: K extends string
		? K extends `$${string}`
			? never
			: T[K] extends Primitive
				? K
				: never
		: never;
}[keyof T];

/* eslint-enable @typescript-eslint/no-explicit-any */
