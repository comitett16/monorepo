import { defineConfig } from '@adonisjs/auth';
import { sessionGuard, sessionUserProvider } from '@adonisjs/auth/session';
import { InferAuthEvents, Authenticators, type InferAuthenticators } from '@adonisjs/auth/types';

const authConfig = defineConfig({
	default: 'web',
	guards: {
		web: sessionGuard({
			useRememberMeTokens: true,
			rememberMeTokensAge: '2w',
			provider: sessionUserProvider({
				model: () => import('#security/user/models/user'),
			}),
		}),
	},
});

export default authConfig;

declare module '@adonisjs/auth/types' {
	interface Authenticators extends InferAuthenticators<typeof authConfig> {}
}
declare module '@adonisjs/core/types' {
	interface EventsList extends InferAuthEvents<Authenticators> {}
}
