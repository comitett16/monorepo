import { defineConfig } from '@adonisjs/inertia';

export default defineConfig({
	rootView: 'inertia_layout',
	sharedData: {
		errors: (ctx) => ctx.session?.flashMessages.get('errors') as Record<string, string> | undefined,
	},
	entrypoint: 'inertia/main.tsx',
	ssr: {
		enabled: false,
		entrypoint: 'inertia/app/ssr.tsx',
	},
});
