import { extractorArbitraryVariants } from '@unocss/extractor-arbitrary-variants';
import {
	defineConfig,
	presetTypography,
	presetUno,
	transformerDirectives,
	transformerVariantGroup,
} from 'unocss';

export default defineConfig({
	content: {
		filesystem: ['inertia/**/*.tsx', 'resources/views/**/*.edge'],
		pipeline: {
			include: ['inertia/**/*.tsx', 'resources/views/**/*.edge'],
		},
	},
	presets: [presetUno(), presetTypography()],
	transformers: [transformerDirectives(), transformerVariantGroup()],
	extractors: [extractorArbitraryVariants],
});
