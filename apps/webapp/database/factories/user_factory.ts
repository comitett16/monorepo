import factory from '@adonisjs/lucid/factories';

import User from '#security/user/models/user';

export const SeasonFactory = factory
	.define(User, () => {
		return {};
	})
	.build();
