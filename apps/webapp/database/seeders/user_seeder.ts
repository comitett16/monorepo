import { BaseSeeder } from '@adonisjs/lucid/seeders';

import User from '#security/user/models/user';

export default class UserSeeder extends BaseSeeder {
	override async run() {
		const users: User[] = [];

		// users.push({
		// 	createdAt: DateTime.now(),
		// 	updatedAt: DateTime.now(),
		// });

		await User.createMany(users);
	}
}
