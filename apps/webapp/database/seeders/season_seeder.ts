import { BaseSeeder } from '@adonisjs/lucid/seeders';
import { DateTime } from 'luxon';

import Season from '#core/season/models/season';

export default class SeasonSeeder extends BaseSeeder {
	override async run() {
		const seasons = [];

		for (const year of [2022, 2023, 2024, 2025]) {
			seasons.push({
				startDate: DateTime.fromObject({ year, month: 7, day: 1 }),
				endDate: DateTime.fromObject({ year: year + 1, month: 6, day: 30 }),
			});
		}

		await Season.createMany(seasons);
	}
}
