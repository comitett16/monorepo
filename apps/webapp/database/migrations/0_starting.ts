import { BaseSchema } from '@adonisjs/lucid/schema';

import { AddressesSchema } from '#database/schemas/addresses';
import { AttachmentsSchema } from '#database/schemas/attachments';
import { ClubContactsSchema } from '#database/schemas/club_contacts';
import { ClubSeasonsSchema } from '#database/schemas/club_seasons';
import { ClubSocialsSchema } from '#database/schemas/club_socials';
import { ClubsSchema } from '#database/schemas/clubs';
import { ClubsLicenseesSchema } from '#database/schemas/clubs_licensees';
import { GradesSchema } from '#database/schemas/grades';
import { GradesLicenseesSchema } from '#database/schemas/grades_licensees';
import { HallsSchema } from '#database/schemas/halls';
import { InvitationsSchema } from '#database/schemas/invitations';
import { LicenseesSchema } from '#database/schemas/licensees';
import { PositionTitlesSchema } from '#database/schemas/position_titles';
import { RememberMeTokensSchema } from '#database/schemas/remember_me_tokens';
import { SeasonsSchema } from '#database/schemas/seasons';
import { UsersSchema } from '#database/schemas/users';

export default class StartingPointMigration extends BaseSchema {
	// eslint-disable-next-line @typescript-eslint/require-await
	override async up() {
		void UsersSchema.create(this);
		void SeasonsSchema.create(this);
		void RememberMeTokensSchema.create(this);
		void ClubsSchema.create(this);
		void ClubSeasonsSchema.create(this);
		void LicenseesSchema.create(this);
		void InvitationsSchema.create(this);
		void GradesSchema.create(this);
		void GradesLicenseesSchema.create(this);
		void ClubsLicenseesSchema.create(this);
		void ClubSocialsSchema.create(this);
		void AddressesSchema.create(this);
		void HallsSchema.create(this);
		void PositionTitlesSchema.create(this);
		void ClubContactsSchema.create(this);
		void AttachmentsSchema.create(this);

		void UsersSchema.alter(this);
	}

	// eslint-disable-next-line @typescript-eslint/require-await
	override async down() {
		void UsersSchema.drop(this);
		void SeasonsSchema.drop(this);
		void RememberMeTokensSchema.drop(this);
		void ClubsSchema.drop(this);
		void ClubSeasonsSchema.drop(this);
		void LicenseesSchema.drop(this);
		void InvitationsSchema.drop(this);
		void GradesSchema.drop(this);
		void GradesLicenseesSchema.drop(this);
		void ClubsLicenseesSchema.drop(this);
		void ClubSocialsSchema.drop(this);
		void AddressesSchema.drop(this);
		void HallsSchema.drop(this);
		void PositionTitlesSchema.drop(this);
		void ClubContactsSchema.drop(this);
		void AttachmentsSchema.drop(this);
	}
}
