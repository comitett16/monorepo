import { BaseSchema } from '@adonisjs/lucid/schema';

export const AddressesSchema = {
	$tableName: 'addresses',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.string('address_line1').notNullable();
			table.string('address_line2').nullable().defaultTo(null);
			table.string('address_line3').nullable().defaultTo(null);
			table.specificType('postal_code', 'char(5)').notNullable();
			table.string('city').notNullable();
			table.decimal('latitude', 8, 9).nullable().defaultTo(null);
			table.decimal('longitude', 9, 6).nullable().defaultTo(null);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
