import { BaseSchema } from '@adonisjs/lucid/schema';

export const RememberMeTokensSchema = {
	$tableName: 'remember_me_tokens',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.integer('tokenable_id').notNullable().unsigned();
			table.string('hash').notNullable();
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.timestamp('expires_at').notNullable();

			// Relationships
			table.foreign('tokenable_id').references('users.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['tokenable_id']);
			table.unique(['hash']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
