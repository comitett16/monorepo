import { BaseSchema } from '@adonisjs/lucid/schema';

export const ClubContactsSchema = {
	$tableName: 'club_contacts',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.integer('club_season_id').notNullable().unsigned();
			table.integer('licensee_id').notNullable().unsigned();
			table.integer('position_title_id').notNullable().unsigned();
			table.string('email').nullable().defaultTo(null);
			table.specificType('phone', 'char(10)').nullable().defaultTo(null);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('club_season_id').references('clubs_seasons.id').onDelete('CASCADE');
			table.foreign('licensee_id').references('licensees.id').onDelete('CASCADE');
			table.foreign('position_title_id').references('position_titles.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['club_season_id', 'licensee_id', 'position_title_id']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
