import { BaseSchema } from '@adonisjs/lucid/schema';

export const InvitationsSchema = {
	$tableName: 'invitations',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.integer('licensee_id').notNullable().unsigned();
			table.json('permissions').notNullable().defaultTo(JSON.stringify([]));
			table.timestamp('expires_at').notNullable();
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('licensee_id').references('licensees.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['licensee_id']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
