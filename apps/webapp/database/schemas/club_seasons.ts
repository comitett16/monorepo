import { BaseSchema } from '@adonisjs/lucid/schema';

export const ClubSeasonsSchema = {
	$tableName: 'clubs_seasons',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.integer('club_id').notNullable().unsigned();
			table.integer('season_id').notNullable().unsigned();
			table.string('name').notNullable();
			table.timestamp('validated_at').notNullable();
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('club_id').references('clubs.id').onDelete('CASCADE');
			table.foreign('season_id').references('seasons.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['club_id', 'season_id']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
