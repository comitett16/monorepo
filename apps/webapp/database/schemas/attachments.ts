import { BaseSchema } from '@adonisjs/lucid/schema';

export const AttachmentsSchema = {
	$tableName: 'attachments',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.string('friendly_name').notNullable();
			table.string('filename').notNullable();
			table.string('path').notNullable();
			table.integer('size').notNullable();
			table.string('mime_type').notNullable();
			table.string('alt').nullable().defaultTo(null);
			table.string('legend').nullable().defaultTo(null);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
