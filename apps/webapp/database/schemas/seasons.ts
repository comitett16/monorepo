import { BaseSchema } from '@adonisjs/lucid/schema';

export const SeasonsSchema = {
	$tableName: 'seasons',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.date('start_date').notNullable();
			table.date('end_date').notNullable();
			table.boolean('is_current').notNullable().defaultTo(false);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['is_current']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
