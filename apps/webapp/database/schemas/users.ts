import { BaseSchema } from '@adonisjs/lucid/schema';

export const UsersSchema = {
	$tableName: 'users',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.string('username').notNullable();
			table.string('password').notNullable();
			table.json('permissions').notNullable().defaultTo(JSON.stringify([]));
			table.specificType('reset_password_token', 'char(20)').nullable().defaultTo(null);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['username']);
		});
	},
	alter({ schema }: BaseSchema) {
		return schema.alterTable(this.$tableName, (table) => {
			// Columns
			table.integer('licensee_id').notNullable().unsigned();
			table.integer('created_by_id').nullable().defaultTo(null);
			table.integer('updated_by_id').nullable().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('licensee_id').references('licensees.id').onDelete('CASCADE');
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
