import { BaseSchema } from '@adonisjs/lucid/schema';

export const HallsSchema = {
	$tableName: 'halls',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.specificType('uid', 'char(20)').notNullable();
			table.string('name').notNullable();
			table.integer('club_season_id').notNullable().unsigned();
			table.integer('address_id').notNullable().unsigned();
			table.boolean('is_default_choice').notNullable().defaultTo(false);
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('club_season_id').references('clubs_seasons.id').onDelete('CASCADE');
			table.foreign('address_id').references('addresses.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['uid']);
			table.unique(['club_season_id', 'address_id']);
			table.unique(['club_season_id', 'is_default_choice']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
