import { BaseSchema } from '@adonisjs/lucid/schema';

export const GradesLicenseesSchema = {
	$tableName: 'grades_licensees',
	create({ schema }: BaseSchema) {
		return schema.createTable(this.$tableName, (table) => {
			// Columns
			table.increments('id').notNullable();
			table.integer('grade_id').notNullable().unsigned();
			table.integer('licensee_id').notNullable().unsigned();
			table.timestamp('obtained_at').notNullable();
			table.timestamp('created_at').notNullable();
			table.timestamp('updated_at').notNullable();
			table.integer('created_by_id').nullable().unsigned().defaultTo(null);
			table.integer('updated_by_id').nullable().unsigned().defaultTo(null);

			// Relationships
			table.foreign('created_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('updated_by_id').references('users.id').onDelete('SET NULL');
			table.foreign('grade_id').references('grades.id').onDelete('CASCADE');
			table.foreign('licensee_id').references('licensees.id').onDelete('CASCADE');

			// Indices
			table.primary(['id']);
			table.unique(['grade_id', 'licensee_id']);
		});
	},
	drop({ schema }: BaseSchema) {
		return schema.dropTable(this.$tableName);
	},
};
