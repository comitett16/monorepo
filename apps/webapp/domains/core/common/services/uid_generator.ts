import { customAlphabet } from 'nanoid';

const nanoId = customAlphabet('abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ123456789', 20);

export type Uid = string;

export function generateUid() {
	return nanoId();
}
