import app from '@adonisjs/core/services/app';
import logger from '@adonisjs/core/services/logger';
import sentry from '@sentry/node';
import { nodeProfilingIntegration } from '@sentry/profiling-node';

import env from '#start/env';

export default class ErrorCaptureService {
	static #initialized = false;

	static #init() {
		if (this.#initialized) return;
		this.#initialized = true;

		sentry.init({
			dsn: env.get('SENTRY_DSN'),
			integrations: [nodeProfilingIntegration()],
			tracesSampleRate: 1,
			profilesSampleRate: 1,
		});
	}

	static captureException(error: unknown) {
		this.#init();

		if (app.inProduction) {
			sentry.captureException(error);
		} else {
			logger.error(error);
		}
	}
}
