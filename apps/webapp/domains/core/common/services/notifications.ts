import { randomUUID, UUID } from 'node:crypto';

type NotificationType = 'info' | 'success' | 'warning' | 'error';
type SessionNotification = {
	id: UUID;
	type: NotificationType;
	message: string;
};

export default class Notifications {
	#notifications: SessionNotification[] = [];

	add(type: NotificationType, message: string) {
		this.#notifications.push({
			id: randomUUID(),
			type,
			message,
		});
	}

	getAll() {
		return this.#notifications;
	}

	clear() {
		this.#notifications = [];
	}
}
