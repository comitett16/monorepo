import type { HttpContext } from '@adonisjs/core/http';
import type { PageObject } from '@adonisjs/inertia/types';

export default interface ControllerInterface {
	handle(httpContext: HttpContext): Promise<string | PageObject | void>;
}
