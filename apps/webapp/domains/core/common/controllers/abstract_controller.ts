import type { HttpContext } from '@adonisjs/core/http';

type InertiaRenderer = Record<string, unknown> | undefined;

export default abstract class AbstractController {
	protected createInertiaResponse(httpContext: HttpContext, component: string) {
		return {
			render: <T extends InertiaRenderer>(data?: T, status?: number) => {
				if (status) {
					httpContext.response.status(status);
				}

				return this.#renderInertia(httpContext, component, data);
			},
		};
	}

	#renderInertia<Payload extends InertiaRenderer>(
		httpContext: HttpContext,
		componentName: string,
		data?: Payload,
	) {
		return httpContext.inertia.render(componentName, {
			data: data ?? null,
			notifications: httpContext.notifications.getAll(),
		});
	}
}
