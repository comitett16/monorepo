import type { Request, Response } from '@adonisjs/core/http';
import type { UUID } from 'node:crypto';

// Validate format of a v7 UUID : `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`
export const uuidRegex = new RegExp(/^[\dA-Fa-f]{8}(?:-[\dA-Fa-f]{4}){3}-[\dA-Fa-f]{12}$/, 'gm');

export function getRequestUuidOrFail(request: Request, response: Response) {
	const parameter = request.param('uuid', null) as UUID | null;

	if (parameter === null) {
		response.abort(400);
	}

	return parameter;
}
