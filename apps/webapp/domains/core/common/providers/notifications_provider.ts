import type { ApplicationService } from '@adonisjs/core/types';

import { HttpContext } from '@adonisjs/core/http';

import Notifications from '#common/services/notifications';

export default class NotificationsProvider {
	constructor(protected app: ApplicationService) {}

	boot() {
		HttpContext.getter(
			'notifications',
			() => {
				return new Notifications();
			},
			true,
		);
	}
}

declare module '@adonisjs/core/http' {
	export interface HttpContext {
		notifications: Notifications;
	}
}
