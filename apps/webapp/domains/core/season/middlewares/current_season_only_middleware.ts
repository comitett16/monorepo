import type { HttpContext } from '@adonisjs/core/http';
import type { NextFn } from '@adonisjs/core/types/http';

import vine from '@vinejs/vine';
import { DateTime } from 'luxon';

import Season from '#core/season/models/season';

const qsSchema = vine.compile(
	vine.object({
		season_id: vine
			.string()
			.uuid({ version: [4] })
			.optional(),
	}),
);

export default class CurrentSeasonOnlyMiddleware {
	async handle({ request, response }: HttpContext, next: NextFn) {
		const now = DateTime.now().toJSDate();
		const currentSeason = await Season.query()
			.where('startDate', '<', now)
			.andWhere('endDate', '>', now)
			.first();

		if (currentSeason === null) {
			return response.badRequest({ message: 'Aucune saison en cours.' });
		}

		const queryParameters = await qsSchema.validate(request.qs());

		if (queryParameters.season_id && queryParameters.season_id !== currentSeason.uid) {
			return response.badRequest({
				message:
					"La saison sélectionnée n'est pas la saison en cours. Toutes les actions de modification sont interdites.",
			});
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return next();
	}
}
