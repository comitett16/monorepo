import type { ApplicationService } from '@adonisjs/core/types';

import { errors } from '@adonisjs/core';
import { HttpContext } from '@adonisjs/core/http';
import { DateTime } from 'luxon';

import Season from '#core/season/models/season';

export default class CurrentSeasonProvider {
	constructor(protected app: ApplicationService) {}

	boot() {
		HttpContext.getter('season', () => {
			const now = DateTime.now().toJSDate();
			const query = Season.query()
				.where('startDate', '<', now)
				.andWhere('endDate', '>', now)
				.first();

			return {
				async getCurrentOrFail() {
					const season = await query;

					if (season === null) {
						throw new errors.E_HTTP_REQUEST_ABORTED('No current season');
					}

					return season;
				},
				getCurrent() {
					return query;
				},
			};
		});
	}
}

declare module '@adonisjs/core/http' {
	export interface HttpContext {
		season: {
			getCurrentOrFail(): Promise<Season>;
			getCurrent(): Promise<Season | null>;
		};
	}
}
