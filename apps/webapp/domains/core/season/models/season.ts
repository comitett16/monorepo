import type { BelongsTo } from '@adonisjs/lucid/types/relations';
import type { DateTime } from 'luxon';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import User from '#security/user/models/user';

export default class Season extends AbstractModel {
	static override table = 'seasons';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column.date()
	declare startDate: DateTime;

	@column.date()
	declare endDate: DateTime;

	@column()
	declare isCurrent: boolean;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(season: Season) {
		season.uid = generateUid();
	}
}
