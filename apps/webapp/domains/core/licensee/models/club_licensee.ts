import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import type { BelongsTo } from '@adonisjs/lucid/types/relations';
import type { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import User from '#security/user/models/user';
import ClubSeason from '#club/models/club_season';
import Licensee from '#core/licensee/models/licensee';

export default class ClubLicensee extends AbstractModel {
	static override table = 'clubs_licensees';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare clubSeasonId: number;

	@belongsTo(() => ClubSeason)
	declare clubSeason: BelongsTo<typeof ClubSeason>;

	@column()
	declare licenseeId: number;

	@belongsTo(() => Licensee)
	declare licensee: BelongsTo<typeof Licensee>;

	@column.dateTime()
	declare transferredAt: DateTime;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(clubLicensee: ClubLicensee) {
		clubLicensee.uid = generateUid();
	}
}
