import { beforeCreate, belongsTo, column, computed, hasMany, hasOne } from '@adonisjs/lucid/orm';
import type { BelongsTo, HasMany, HasOne } from '@adonisjs/lucid/types/relations';
import type { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import ClubLicensee from '#core/licensee/models/club_licensee';
import GradeLicensee from '#core/licensee/models/grade_licensee';
import User from '#security/user/models/user';

export default class Licensee extends AbstractModel {
	static override table = 'licensees';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare code: string;

	@column()
	declare firstname: string;

	@column()
	declare lastname: string;

	@column()
	declare gender: string;

	@column()
	declare email: string | null;

	@column()
	declare phone: string | null;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@hasMany(() => ClubLicensee)
	declare clubLicensees: HasMany<typeof ClubLicensee>;

	@hasMany(() => GradeLicensee)
	declare gradeLicensees: HasMany<typeof GradeLicensee>;

	@hasOne(() => User)
	declare user: HasOne<typeof User> | null;

	@beforeCreate()
	static generateUid(licensee: Licensee) {
		licensee.uid = generateUid();
	}

	@computed()
	get fullName() {
		return `${this.firstname} ${this.lastname}`;
	}
}
