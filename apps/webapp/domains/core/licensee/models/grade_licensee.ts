import { belongsTo, column } from '@adonisjs/lucid/orm';
import type { BelongsTo } from '@adonisjs/lucid/types/relations';
import type { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import Grade from '#core/licensee/models/grade';
import Licensee from '#core/licensee/models/licensee';
import User from '#security/user/models/user';

export default class GradeLicensee extends AbstractModel {
	static override table = 'grades_licensees';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare gradeId: number;

	@belongsTo(() => Grade)
	declare grade: BelongsTo<typeof Grade>;

	@column()
	declare licenseeId: number;

	@belongsTo(() => Licensee)
	declare licensee: BelongsTo<typeof Licensee>;

	@column.dateTime()
	declare obtainedAt: DateTime;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;
}
