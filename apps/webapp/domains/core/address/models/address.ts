import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import User from '#security/user/models/user';

export default class Address extends AbstractModel {
	static override table = 'addresses';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare addressLine1: string;

	@column()
	declare addressLine2: string | null;

	@column()
	declare addressLine3: string | null;

	@column()
	declare postalCode: string;

	@column()
	declare city: string;

	@column()
	declare latitude: string | null;

	@column()
	declare longitude: string | null;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(address: Address) {
		address.uid = generateUid();
	}
}
