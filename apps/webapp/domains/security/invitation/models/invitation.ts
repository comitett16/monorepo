import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import Licensee from '#core/licensee/models/licensee';
import User from '#security/user/models/user';
import { Permission } from '#security/user/value_objects/security';

export default class Invitation extends AbstractModel {
	static override table = 'invitations';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare licenseeId: number;

	@belongsTo(() => Licensee)
	declare licensee: BelongsTo<typeof Licensee>;

	@column()
	declare permissions: Permission[];

	@column.dateTime()
	declare expiresAt: DateTime;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(invitation: Invitation) {
		invitation.uid = generateUid();
	}

	@beforeCreate()
	static assignExpirationDate(invitation: Invitation) {
		invitation.expiresAt = DateTime.now().plus({ days: 2 });
	}
}
