import { Secret } from '@adonisjs/core/helpers';
import vine, { SimpleMessagesProvider } from '@vinejs/vine';
import { type Infer, ValidationMessages } from '@vinejs/vine/types';

const schema = vine.compile(
	vine.object({
		password: vine
			.string()
			.minLength(8)
			.transform((value) => new Secret(value)),
	}),
);

const messages: ValidationMessages = {
	'required': 'Le champs {{ field }} est obligatoire.',
	'password.minLength': 'Le mot de passe doit contenir au moins 8 caractères.',
};

schema.messagesProvider = new SimpleMessagesProvider(messages);

export default schema;
export type AcceptInvitationSchema = Infer<typeof schema>;
