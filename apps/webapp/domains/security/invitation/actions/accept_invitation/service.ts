import type { AcceptInvitationSchema } from './validator.js';

import { DateTime } from 'luxon';

import type Season from '#core/season/models/season';
import InvitationExpiredException from '#security/invitation/exceptions/invitation_expired';
import type Invitation from '#security/invitation/models/invitation';
import UndefinedLicenseeEmailException from '#security/user/exceptions/undefined_licensee_email';
import User from '#security/user/models/user';

type ActionParameters = {
	invitation: Invitation;
	payload: AcceptInvitationSchema;
	currentSeason: Season;
};

export default class AcceptInvitationService {
	async execute({ invitation, currentSeason, payload }: ActionParameters) {
		if (invitation.expiredAt < DateTime.now()) {
			throw new InvitationExpiredException();
		}

		await invitation.load((preloader) => {
			// @see https://github.com/adonisjs/lucid/issues/1008
			// @ts-expect-error This relation is defined in the abstract model.
			preloader.preload('createdBy');
			preloader.preload('licensee');
		});

		if (invitation.licensee.email === null) {
			throw new UndefinedLicenseeEmailException();
		}

		const user = await User.create({
			identifier: invitation.licensee.email,
			password: payload.password.release(),
			permissions: invitation.permissions,
			seasonId: currentSeason.id,
			createdById: null,
			updatedById: null,
		});

		await invitation.delete();

		return user;
	}
}
