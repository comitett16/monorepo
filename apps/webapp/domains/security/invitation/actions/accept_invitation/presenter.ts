import type User from '#security/user/models/user';

export default class AcceptInvitationPresenter {
	json(user: User) {
		return {
			userId: user.uuid,
		};
	}
}
