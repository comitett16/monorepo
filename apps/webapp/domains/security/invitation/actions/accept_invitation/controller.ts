import type AcceptInvitationPresenter from './presenter.js';
import type AcceptInvitationAction from './service.js';
import type { HttpContext } from '@adonisjs/core/http';

import { inject } from '@adonisjs/core';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';
import { getRequestUuidOrFail } from '#common/controllers/uuid_param';
import Invitation from '#security/invitation/models/invitation';

import schema from './validator.js';

@inject()
export default class AcceptInvitationController
	extends AbstractController
	implements ControllerInterface
{
	constructor(
		private readonly action: AcceptInvitationAction,
		private readonly presenter: AcceptInvitationPresenter,
	) {
		super();
	}

	async handle({ request, response, season }: HttpContext) {
		const currentSeason = await season.getCurrentOrFail();
		const uuid = getRequestUuidOrFail(request, response);

		const invitation = await Invitation.findByOrFail('uuid', uuid);

		const payload = await request.validateUsing(schema);
		const result = await this.action.execute({ invitation, currentSeason, payload });

		return response.created(this.presenter.json(result));
	}
}
