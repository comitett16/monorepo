import type { CreateInvitationSchema } from './validator.js';

import type Season from '#core/season/models/season';
import Invitation from '#security/invitation/models/invitation';
import UserAlreadyExistsException from '#security/user/exceptions/user_already_exists';
import type User from '#security/user/models/user';

export default class CreateInvitationService {
	async execute(payload: CreateInvitationSchema, season: Season, authUser: User) {
		// const existingUser = await UserRepository.findOneBy('identifier', payload.licence);
		const existingUser = null;

		if (existingUser !== null) {
			throw new UserAlreadyExistsException();
		}

		return Invitation.create({
			...payload,
			seasonId: season.id,
			createdById: authUser.id,
			updatedById: authUser.id,
		});
	}
}
