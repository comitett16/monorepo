import type Invitation from '#security/invitation/models/invitation';

export default class CreateInvitationPresenter {
	json(invitation: Invitation) {
		return {
			invitationId: invitation.uuid,
		};
	}
}
