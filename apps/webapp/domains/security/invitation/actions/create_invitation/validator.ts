import vine, { SimpleMessagesProvider } from '@vinejs/vine';
import { type Infer, ValidationMessages } from '@vinejs/vine/types';

import { permissions } from '#security/user/value_objects/security';

const schema = vine.compile(
	vine.object({
		licence: vine.string().regex(/^\d+$/),
		email: vine.string().email(),
		permissions: vine.array(vine.literal(permissions)),
	}),
);

const messages: ValidationMessages = {
	'required': 'Le champs {{ field }} est obligatoire.',
	'email.email': "Le format de l'adresse email n'est pas valide.",
	'permissions.array': "L'invitation doit contenir au moins une permission.",
	'permissions.array.*.string': "La permission {{ value }} n'est pas valide.",
};

schema.messagesProvider = new SimpleMessagesProvider(messages);

export default schema;
export type CreateInvitationSchema = Infer<typeof schema>;
