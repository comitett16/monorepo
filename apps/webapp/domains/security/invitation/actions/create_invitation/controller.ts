import type CreateInvitationPresenter from './presenter.js';
import type CreateInvitationService from './service.js';
import type { HttpContext } from '@adonisjs/core/http';

import { inject } from '@adonisjs/core';

import schema from './validator.js';

@inject()
export default class CreateInvitationController {
	constructor(
		private readonly action: CreateInvitationService,
		private readonly presenter: CreateInvitationPresenter,
	) {}

	async handle({ auth, request, response, season }: HttpContext) {
		const authUser = auth.getUserOrFail();
		const currentSeason = await season.getCurrentOrFail();
		const payload = await request.validateUsing(schema);
		const invitation = await this.action.execute(payload, currentSeason, authUser);
		return response.created(this.presenter.json(invitation));
	}
}
