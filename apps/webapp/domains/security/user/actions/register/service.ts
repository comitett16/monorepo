import type { RegisterSchema } from './validator.js';

import UserAlreadyExistsException from '#security/user/exceptions/user_already_exists';
import User from '#security/user/models/user';

export default class RegisterService {
	async execute(payload: RegisterSchema) {
		// const existingUser = await UserRepository.findOneBy('identifier', payload.identifier);
		const existingUser = null;

		if (existingUser !== null) {
			throw new UserAlreadyExistsException();
		}

		return User.create(payload);
	}
}
