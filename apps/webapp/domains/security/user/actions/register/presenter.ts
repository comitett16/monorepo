import AbstractPresenter from '#common/presenters/abstract_presenter';
import User from '#security/user/models/user';

export default class RegisterPresenter extends AbstractPresenter {
	inertia(user: User) {
		return {
			userId: user.uuid,
		};
	}
}
