import { Bouncer } from '@adonisjs/bouncer';
import { inject } from '@adonisjs/core';
import { HttpContext } from '@adonisjs/core/http';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';
import RegisterPresenter from '#security/user/actions/register/presenter';

import RegisterService from './service.js';
import schema from './validator.js';

const ability = Bouncer.ability({ allowGuest: false }, () => true);

@inject()
export default class RegisterController extends AbstractController implements ControllerInterface {
	constructor(
		private readonly action: RegisterService,
		private readonly presenter: RegisterPresenter,
	) {
		super();
	}

	async handle(httpContext: HttpContext) {
		const { auth, bouncer, notifications, response, request } = httpContext;

		if (await bouncer.denies(ability)) {
			notifications.add('error', 'You are not allowed to perform this action');

			return response.redirect().back();
		}

		const payload = await request.validateUsing(schema);
		const { render } = this.createInertiaResponse(httpContext, 'home');

		const user = await this.action.execute(payload);
		await auth.use('web').login(user);

		return render(this.presenter.inertia(user));
	}
}
