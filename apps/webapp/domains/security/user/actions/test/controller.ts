import { HttpContext } from '@adonisjs/core/http';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';
import { generateUid } from '#common/services/uid_generator';

export default class RegisterController extends AbstractController implements ControllerInterface {
	async handle(httpContext: HttpContext) {
		return httpContext.inertia.render('home', {
			data: {
				userId: generateUid(),
				firstname: 'John',
			},
			notifications: [],
		});
	}
}
