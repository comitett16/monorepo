import { inject } from '@adonisjs/core';
import { HttpContext } from '@adonisjs/core/http';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';

@inject()
export default class LoginFormController extends AbstractController implements ControllerInterface {
	handle(httpContext: HttpContext) {
		const { render } = this.createInertiaResponse(httpContext, 'security/login');
		return render();
	}
}
