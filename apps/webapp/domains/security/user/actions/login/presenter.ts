import AbstractPresenter from '#common/presenters/abstract_presenter';
import type User from '#security/user/models/user';

export default class LoginPresenter extends AbstractPresenter {
	inertia(user: User) {
		return {
			userId: user.uuid,
		};
	}
}
