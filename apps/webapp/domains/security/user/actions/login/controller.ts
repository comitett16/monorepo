import { inject } from '@adonisjs/core';
import { HttpContext } from '@adonisjs/core/http';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';
import User from '#security/user/models/user';

import schema from './validator.js';

@inject()
export default class LoginController extends AbstractController implements ControllerInterface {
	async handle(httpContext: HttpContext) {
		const { auth, request, response } = httpContext;
		const payload = await request.validateUsing(schema);
		console.log({ payload });
		const user = await User.verifyCredentials(payload.identifier, payload.password);

		await auth.use('web').login(user, payload.remember);

		return response.redirect().toRoute('inertia.test');
	}
}
