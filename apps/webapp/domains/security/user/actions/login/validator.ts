import vine, { SimpleMessagesProvider } from '@vinejs/vine';
import { type Infer, ValidationMessages } from '@vinejs/vine/types';

const schema = vine.compile(
	vine.object({
		identifier: vine.string().regex(/^\d+$/),
		password: vine.string().minLength(8),
		remember: vine.boolean().optional(),
	}),
);

const messages: ValidationMessages = {
	'required': 'Le champs {{ field }} est obligatoire.',
	'identifier.regex': 'Le numéro de licence doit être un nombre.',
	'password.minLength': 'Le mot de passe doit contenir au moins 8 caractères.',
};

schema.messagesProvider = new SimpleMessagesProvider(messages);

export default schema;
export type LoginSchema = Infer<typeof schema>;
