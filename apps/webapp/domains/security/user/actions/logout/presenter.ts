import AbstractPresenter from '#common/presenters/abstract_presenter';

export default class LogoutPresenter extends AbstractPresenter {
	json() {
		return {};
	}
}
