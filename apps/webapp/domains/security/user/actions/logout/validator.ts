import vine, { SimpleMessagesProvider } from '@vinejs/vine';
import { type Infer, ValidationMessages } from '@vinejs/vine/types';

const schema = vine.compile(vine.object({}));

const messages: ValidationMessages = {
	required: 'Le champs {{ field }} est obligatoire.',
};

schema.messagesProvider = new SimpleMessagesProvider(messages);

export default schema;
export type LogoutSchema = Infer<typeof schema>;
