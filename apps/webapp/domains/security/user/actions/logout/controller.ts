import { HttpContext } from '@adonisjs/core/http';

import AbstractController from '#common/controllers/abstract_controller';
import ControllerInterface from '#common/controllers/controller_interface';

export default class LogoutController extends AbstractController implements ControllerInterface {
	async handle({ auth, response }: HttpContext) {
		await auth.use('web').logout();

		return response.noContent();
	}
}
