import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { withAuthFinder } from '@adonisjs/auth/mixins/lucid';
import { DbRememberMeTokensProvider } from '@adonisjs/auth/session';
import { compose } from '@adonisjs/core/helpers';
import hash from '@adonisjs/core/services/hash';
import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import Licensee from '#core/licensee/models/licensee';
import { Permission } from '#security/user/value_objects/security';

const AuthFinder = withAuthFinder(() => hash.use('scrypt'), {
	uids: ['username'],
	passwordColumnName: 'password',
});

export default class User extends compose(AbstractModel, AuthFinder) {
	static override table = 'users';
	static rememberMeTokens = DbRememberMeTokensProvider.forModel(User);

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare username: string;

	@column()
	declare password: string;

	@column()
	declare permissions: Permission[];

	@column()
	declare resetPasswordToken: string | null;

	@column()
	declare licenseeId: number;

	@belongsTo(() => Licensee)
	declare licensee: BelongsTo<typeof Licensee>;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(user: User) {
		user.uid = generateUid();
	}
}
