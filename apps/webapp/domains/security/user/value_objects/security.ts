export type UserRole = 'committee_member' | 'licensee' | 'club_admin' | 'referee';

export const permissions = {
	committee: {
		'users.index': 'com:users.index',
		'users.show': 'com:users.show',
		'users.update': 'com:users.update',
		'users.delete': 'com:users.delete',
		'invitation.create': 'com:invitation.create',
		'invitation.show': 'com:invitation.show',
	},
	club: {},
	player: {},
	referee: {},
} as const;

// eslint-disable-next-line unicorn/no-array-reduce
const flatPermissions = Object.entries(permissions).reduce<string[]>((acc, [, value]) => {
	for (const [, v] of Object.entries(value)) {
		acc.push(v);
	}

	return acc;
}, []);

export type Permission = typeof flatPermissions;
export type CommitteePermission = (typeof permissions)['committee'];
export type ClubPermission = (typeof permissions)['club'];
export type PlayerPermission = (typeof permissions)['player'];
export type RefereePermission = (typeof permissions)['referee'];
