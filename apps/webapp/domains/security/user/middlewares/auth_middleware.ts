import type { HttpContext } from '@adonisjs/core/http';
import type { NextFn } from '@adonisjs/core/types/http';

export default class AuthMiddleware {
	handle(ctx: HttpContext, next: NextFn) {
		if (!ctx.auth.isAuthenticated) {
			ctx.response.unauthorized({ message: 'Unauthorized' });
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return next();
	}
}
