import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import ClubSeason from '#club/models/club_season';
import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import Licensee from '#core/licensee/models/licensee';
import PositionTitle from '#core/position_title/models/position_title';
import User from '#security/user/models/user';

export default class ClubContact extends AbstractModel {
	static override table = 'club_contacts';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare clubSeasonId: number;

	@belongsTo(() => ClubSeason)
	declare clubSeason: BelongsTo<typeof ClubSeason>;

	@column()
	declare licenseeId: number;

	@belongsTo(() => Licensee)
	declare licensee: BelongsTo<typeof Licensee>;

	@column()
	declare positionTitleId: number;

	@belongsTo(() => PositionTitle)
	declare positionTitle: BelongsTo<typeof PositionTitle>;

	@column()
	declare email: string | null;

	@column()
	declare phone: string | null;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(clubContact: ClubContact) {
		clubContact.uid = generateUid();
	}
}
