import type { BelongsTo, HasMany } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column, hasMany } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import ClubSeason from '#club/models/club_season';
import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import User from '#security/user/models/user';

export default class Club extends AbstractModel {
	static override table = 'clubs';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare federationId: string;

	@column()
	declare code: string;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@hasMany(() => ClubSeason)
	declare clubSeasons: HasMany<typeof ClubSeason>;

	@beforeCreate()
	static generateUid(club: Club) {
		club.uid = generateUid();
	}
}
