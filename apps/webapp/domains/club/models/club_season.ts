import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import Club from '#club/models/club';
import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import Season from '#core/season/models/season';
import User from '#security/user/models/user';

export default class ClubSeason extends AbstractModel {
	static override table = 'clubs_seasons';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare clubId: number;

	@belongsTo(() => Club)
	declare club: BelongsTo<typeof Club>;

	@column()
	declare seasonId: number;

	@belongsTo(() => Season)
	declare season: BelongsTo<typeof Season>;

	@column()
	declare name: string;

	@column.date()
	declare validatedAt: DateTime;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(clubSeason: ClubSeason) {
		clubSeason.uid = generateUid();
	}
}
