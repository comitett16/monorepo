import type { BelongsTo } from '@adonisjs/lucid/types/relations';

import { beforeCreate, belongsTo, column } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';

import ClubSeason from '#club/models/club_season';
import AbstractModel from '#common/models/abstract_model';
import type { Uid } from '#common/services/uid_generator';
import { generateUid } from '#common/services/uid_generator';
import Address from '#core/address/models/address';
import User from '#security/user/models/user';

export default class Hall extends AbstractModel {
	static override table = 'halls';

	@column({ isPrimary: true })
	declare id: number;

	@column()
	declare uid: Uid;

	@column()
	declare name: string;

	@column()
	declare clubSeasonId: number;

	@belongsTo(() => ClubSeason)
	declare clubSeason: BelongsTo<typeof ClubSeason>;

	@column()
	declare addressId: number;

	@belongsTo(() => Address)
	declare address: BelongsTo<typeof Address>;

	@column()
	declare isDefaultChoice: boolean;

	@column.dateTime({ autoCreate: true })
	declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	declare updatedAt: DateTime;

	@column()
	declare createdById: number | null;

	@belongsTo(() => User)
	declare createdBy: BelongsTo<typeof User> | null;

	@column()
	declare updatedById: number | null;

	@belongsTo(() => User)
	declare updatedBy: BelongsTo<typeof User> | null;

	@beforeCreate()
	static generateUid(hall: Hall) {
		hall.uid = generateUid();
	}
}
