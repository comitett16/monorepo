export function booleanToNumber(value: boolean) {
		return value ? 1 : 0;
}
