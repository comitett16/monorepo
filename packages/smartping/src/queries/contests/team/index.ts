import { getMatch } from './get_match.js';
import { getPoolsForDivision } from './get_pools.js';
import { getPoolRanking } from './get_pool_ranking.js';
import { getPoolInitialOrder } from './get_pool_initial_order.js';
import { getMatchesForPool } from './get_pool_matches.js';

export default {
	getMatch,
	getMatchesForPool,
	getPoolsForDivision,
	getPoolRanking,
	getPoolInitialOrder,
};
